package com.unju.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class controller {

	@RequestMapping(method = RequestMethod.GET, value = "/saludo")
	@ResponseBody
	public String ejemplo() {
		return "<h1>hola a todos los de PV 2023</h1>";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/pv2023")
	@ResponseBody
	public String ejemplo2() {
		return "<h2>Programacion Visual</h2>";
	}
}
