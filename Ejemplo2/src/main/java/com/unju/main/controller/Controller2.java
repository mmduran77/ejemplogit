package com.unju.main.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.websocket.server.PathParam;

@RestController
public class Controller2 {


	@GetMapping("/saludo2")
	public String ejemplo3() {
		return "<h1>Bienvenido a RestController </h1>";
	}
	
	@GetMapping("/saludo2/{nombre}")
	public String ejemplo4(@PathVariable ("nombre") String nom) {
		return "<h2>Bienvenido " + nom + " a Programacion Visual</h2>"
				+ "<a href=''>Ir a ... </a>";
	}
	
	//Calculo de la funcion factorial
	@GetMapping("/factorial/{num}")
	public String factorial(@PathVariable ("num") int  num) {
		int fac = 1;
		int aux = num;
		if(num<0) {
			return "No se puede calcular el factorial de un Nro negativo";
		}
		while (aux != 0) {
			fac = fac * aux;
			aux--;
		}
		
		return ("El factorial de " + num + " es: " + fac);
	}
}